package parking.store;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.repackaged.org.codehaus.jackson.annotate.JsonIgnore;

public interface IStoreEntity {
    @JsonIgnore
    Entity getEntity();
}
