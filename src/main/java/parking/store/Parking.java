package parking.store;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import java.util.Date;

public class Parking  implements IStoreEntity{


    public static final String FROM = "FROM";
    public static final String TO = "TO";
    public static final String OUTDATE = "OUTDATE";
    public static String KIND = "parking";

    private Key key;

    // кто блокирует
    private String fromUserKey;

    // кого блокирует
    private String toUserKey;

    //когда покинет парковку
    private Date outTime;

    public Parking() {
    }

    public Parking(Entity entity) {
        key = entity.getKey();
        fromUserKey = (String)entity.getProperty(FROM);
        toUserKey = (String)entity.getProperty(TO);
        outTime = (Date) entity.getProperty(OUTDATE);
    }

    public Parking(String from, String to, Date time) {
        this.fromUserKey = from;
        this.toUserKey = to;
        this.outTime = time;
    }

    public Parking(String from, Date time) {
        this(from, null, time);
    }

    public Key getKey() {
        return key;
    }

    public String getFromUserKey() {
        return fromUserKey;
    }

    public void setFromUserKey(String fromUserKey) {
        this.fromUserKey = fromUserKey;
    }

    public String getToUserKey() {
        return toUserKey;
    }

    public void setToUserKey(String toUserKey) {
        this.toUserKey = toUserKey;
    }

    public Date getOutTime() {
        return outTime;
    }

    public void setOutTime(Date outTime) {
        this.outTime = outTime;
    }

    @Override
    public Entity getEntity() {
        Entity entity = (key == null) ?  new Entity(KIND) : new Entity(KIND, key.getId());
        entity.setProperty(FROM, getFromUserKey());
        if(getToUserKey() != null) {
            entity.setProperty(TO, getToUserKey());
        }
        entity.setProperty(OUTDATE, getOutTime());
        return entity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Parking parking = (Parking) o;

        if (fromUserKey != null ? !fromUserKey.equals(parking.fromUserKey) : parking.fromUserKey != null) return false;
        if (toUserKey != null ? !toUserKey.equals(parking.toUserKey) : parking.toUserKey != null) return false;
        return outTime != null ? outTime.equals(parking.outTime) : parking.outTime == null;
    }

    @Override
    public int hashCode() {
        int result = fromUserKey != null ? fromUserKey.hashCode() : 0;
        result = 31 * result + (toUserKey != null ? toUserKey.hashCode() : 0);
        result = 31 * result + (outTime != null ? outTime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Parking{" +
                "key=" + key +
                ", fromUserKey='" + fromUserKey + '\'' +
                ", toUserKey='" + toUserKey + '\'' +
                ", outTime=" + outTime +
                '}';
    }
}
