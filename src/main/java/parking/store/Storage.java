package parking.store;

import com.google.appengine.api.datastore.*;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;


public class Storage {

    private DatastoreService datastore;

    public Storage(){
        datastore = DatastoreServiceFactory.getDatastoreService();
    }

    public String putUser(User user) {
        return putEntity(user);
    }

    public User getUser(String keyStr)  {
        final Key key = KeyFactory.stringToKey(keyStr);
        Entity userEntity = null;
        try {
            userEntity = datastore.get(key);
        } catch (EntityNotFoundException e) {
            return null;
        }
        return new User(userEntity);
    }

    public void deleteEntityByKey(String userKey){
        final Key key = KeyFactory.stringToKey(userKey);
        datastore.delete(key);
    }

    public String putParking(Parking parking){
        return putEntity(parking);
    }


    public Parking getParking(String parkingKey)  {
        final Key key = KeyFactory.stringToKey(parkingKey);
        Entity entity = null;
        try {
            entity = datastore.get(key);
        } catch (EntityNotFoundException e) {
            return null;
        }
        return new Parking(entity);
    }

    /*
    *   кто блокирует машину с userKey
    *
    *   выезд  M1 - M2 - M3 - M4 - M5 - M6 - M7 тупик
    *
    *   для M3 это M2
    *
    * */

    public String getFromParkings(String userKey) {
        Filter filter =
                new FilterPredicate(Parking.TO, FilterOperator.EQUAL, userKey);
        Query q = new Query(Parking.KIND).setFilter(filter);
        PreparedQuery pq = datastore.prepare(q);
        final Entity entity = pq.asSingleEntity();
        if (entity == null){
            return null;
        }
        return (String) entity.getProperty(Parking.FROM);
    }

    /*  для userKey получить кого он блокирует
    *
    *   выезд  M1 - M2 - M3 - M4 - M5 - M6 - M7 тупик
    *
    *   для M3 это M4
    *
    */

    public String getToParkings(String userKey){
        Filter filter =
                new FilterPredicate(Parking.FROM, FilterOperator.EQUAL, userKey);
        Query q = new Query(Parking.KIND).setFilter(filter);
        PreparedQuery pq = datastore.prepare(q);
        Entity entity = pq.asSingleEntity();
        if (entity == null){
            return null;
        }
        return (String) entity.getProperty(Parking.TO);
    }


    private String putEntity(IStoreEntity storeEntity) {
        final Entity entity = storeEntity.getEntity();
        final Key key = datastore.put(entity);
        return KeyFactory.keyToString(key);
    }


    public void deleteParking(String userKey){
        Filter filter =
                new FilterPredicate(Parking.FROM, FilterOperator.EQUAL, userKey);
        Query q = new Query(Parking.KIND).setFilter(filter);
        PreparedQuery pq = datastore.prepare(q);
        Entity entity = pq.asSingleEntity();
        if(entity != null){
            datastore.delete(entity.getKey());
        }
    }

    /**
     * парковка пользователя
     *
     * */
    public Parking getUserParking(String userKey) {
        Filter filter =
                new FilterPredicate(Parking.FROM, FilterOperator.EQUAL, userKey);
        Query q = new Query(Parking.KIND).setFilter(filter);
        PreparedQuery pq = datastore.prepare(q);
        Entity entity = pq.asSingleEntity();
        if (entity == null){
            return null;
        }
        return new Parking(entity);
    }
}
