package parking.store;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

public class User implements IStoreEntity {

    public static final String SUBSCRIBEURL = "subscribeurl";
    public static final String PUBLICKEY = "publickey";
    public static String KIND  = "user";
    public static final String NAME = "name";
    public static final String PHONE = "phone";
    public static final String AVTO_NUMBER = "avtoNumber";

    private Key key;
    private String name;
    private String phoneNumber;
    private String avtoNumber;
    private String subscribeURL;
    private String publicKey;

    public User(){}

    public User(String name, String phoneNumber, String avtoNumber, String subscribeURL, String publicKey) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.avtoNumber = avtoNumber;
        this.subscribeURL = subscribeURL;
        this.publicKey = publicKey;
    }

    public User(Entity userEntity) {
        name = (String)userEntity.getProperty(NAME);
        phoneNumber = (String)userEntity.getProperty(PHONE);
        avtoNumber = (String)userEntity.getProperty(AVTO_NUMBER);
        key = userEntity.getKey();
        subscribeURL = (String) userEntity.getProperty(SUBSCRIBEURL);
        publicKey = (String) userEntity.getProperty(PUBLICKEY);
    }

    public String getKey(){
        return KeyFactory.keyToString(key);
    }

    public String getSubscribeURL() {
        return subscribeURL;
    }

    public void setSubscribeURL(String subscribeURL) {
        this.subscribeURL = subscribeURL;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAvtoNumber() {
        return avtoNumber;
    }

    public void setAvtoNumber(String avtoNumber) {
        this.avtoNumber = avtoNumber;
    }

    @Override
    public Entity getEntity() {
        Entity user = (key == null) ?  new Entity(KIND) : new Entity(KIND, key.getId());
        user.setProperty(NAME, getName());
        user.setProperty(PHONE, getPhoneNumber());
        user.setProperty(AVTO_NUMBER, getAvtoNumber());
        user.setProperty(SUBSCRIBEURL,getSubscribeURL());
        user.setProperty(PUBLICKEY,getPublicKey());
        return user;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", avtoNumber='" + avtoNumber + '\'' +
                ", subscribeURL='" + subscribeURL + '\'' +
                ", publicKey='" + publicKey + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(user.phoneNumber) : user.phoneNumber != null) return false;
        if (avtoNumber != null ? !avtoNumber.equals(user.avtoNumber) : user.avtoNumber != null) return false;
        if (subscribeURL != null ? !subscribeURL.equals(user.subscribeURL) : user.subscribeURL != null) return false;
        return publicKey != null ? publicKey.equals(user.publicKey) : user.publicKey == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (avtoNumber != null ? avtoNumber.hashCode() : 0);
        result = 31 * result + (subscribeURL != null ? subscribeURL.hashCode() : 0);
        result = 31 * result + (publicKey != null ? publicKey.hashCode() : 0);
        return result;
    }
}
