package ru.mouseirradiation.smartparking;

import com.google.appengine.api.datastore.EntityNotFoundException;
import parking.store.Storage;
import parking.store.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.logging.Logger;

@Path("/")
public class TestRestEndpoint {
    private static final Logger log = Logger.getLogger(TestRestEndpoint.class.getName());

    @Path("/hello")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getEmployee() {
        log.info("getEmployee Call!");
        return "Hello World!";
    }

/*    @Path("/subscribe")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String subscireUser(@QueryParam("user_id") String userId,
                               @QueryParam("subscribe_url") String subscribeUrl,
                               @QueryParam("user_public_key") String userPubKey) {
        Storage storage = new Storage();
        User user = null;
        try {
            user = storage.getUser(userId);
        } catch (EntityNotFoundException e) {
            user = new User();
        }
        user.setSubscribeURL(subscribeUrl);
        user.setPublicKey(userPubKey);
        storage.putUser(user);
        return storage.putUser(user);
    }*/

    @Path("/subscribe")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String subscribeUser(User user) {
        log.info("subscribeUser() call for User: " + user);
/*        log.info("subscribeUser() call for User: " + user);
        Storage storage = new Storage();
        storage.putUser(user);
        return storage.putUser(user);*/
        return "";
    }
}
