package ru.mouseirradiation.smartparking.rest;

import parking.store.Parking;
import parking.store.Storage;
import parking.store.User;
import ru.mouseirradiation.smartparking.rest.dto.StatusDto;
import ru.mouseirradiation.smartparking.rest.dto.UserParkingDto;

import javax.ws.rs.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SmartParkingEndpoint {
    private static final Logger log = Logger.getLogger(SmartParkingEndpoint.class.getName());

    private static final String SMART_PARKING_USER_ID_COOKIE = "SmartParking_UserId";

    @GET
    @Path("hello")
    public String doHello() {
        return "Hello World!";
    }

    @GET
    @Path("status")
    public StatusDto getStatus(@CookieParam(SMART_PARKING_USER_ID_COOKIE) Cookie cookie) throws Exception {
        log.info("getStatus");
        final StatusDto result = new StatusDto();

        if (cookie != null) {
            final Storage storage = new Storage();

            final String id = cookie.getValue();
            final User user = storage.getUser(id);
            result.setUser(user);

            final Parking parking = storage.getUserParking(id);
            result.setParking(parking);

            final String lockingMeUserId = storage.getFromParkings(id);
            if (lockingMeUserId != null) {
                final User lockingMe = storage.getUser(lockingMeUserId);
                result.setLockingMe(lockingMe);
            }

            final String lockByMeUserId = storage.getToParkings(id);
            if (lockByMeUserId != null) {
                final User lockByMe = storage.getUser(lockByMeUserId);
                result.setLockedByMe(lockByMe);
            }
        }

        return result;
    }

    @GET
    @Path("info/{userId}")
    public UserParkingDto getInfo(@PathParam("userId") String userId) throws Exception {
        log.info("getInfo: " + userId);

        final Storage storage = new Storage();
        final User user = storage.getUser(userId);
        final Parking parking = storage.getUserParking(userId);

        return new UserParkingDto(user,parking);
    }

    @POST
    @Path("parking")
    public void doParking(@CookieParam(SMART_PARKING_USER_ID_COOKIE) Cookie cookie,
                          Parking parking) throws Exception {
        log.info("doParking: " + parking);
        final Storage storage = new Storage();

        final String id = cookie.getValue();
        final User user = storage.getUser(id);
        if (user == null) {
            throw new IllegalStateException("User not found. id=" + id);
        }

        final Parking previousParking = storage.getUserParking(id);
        if (previousParking != null) {
            throw new IllegalStateException("Нельзя запарковаться дважды!");
        }

        parking.setFromUserKey(user.getKey());
        storage.putParking(parking);
    }

    @POST
    @Path("leave")
    public StatusDto doLeave(@CookieParam(SMART_PARKING_USER_ID_COOKIE) Cookie cookie) throws Exception {
        log.info("doLeave");
        final Storage storage = new Storage();
        final String id = cookie.getValue();
        //final User user = storage.getUser(cookie.getValue());

        final Parking previousParking = storage.getUserParking(id);
        if (previousParking == null) {
            throw new IllegalStateException("Нельзя распарковаться если ты не запаркован!");
        }

        final StatusDto result = getStatus(cookie);

        if(result.getLockingMe() == null) {
            storage.deleteParking(id);
        }

        return result;

//        final String lockingMe = storage.getFromParkings(id);
//        if (lockingMe != null) {
//            // final User lockingMeUser = storage.getUserParking(lockingMe);
//            throw new IllegalStateException("Нельзя распарковаться если ты заблокирован!");
//        }
//        storage.deleteParking(id);
    }

    @POST
    @Path("notify/{number}")
    public void doNotify(@CookieParam(SMART_PARKING_USER_ID_COOKIE) Cookie cookie,
                         @PathParam("number") String number) {
        log.info("doNotify: " + number);
    }

    @POST
    @Path("register")
    public Response doRegister(@CookieParam(SMART_PARKING_USER_ID_COOKIE) Cookie cookie,
                               User user) {
        log.info("doRegister: " + user);
        log.info("cookie: " + cookie);

        final Storage storage = new Storage();
        final String userId;
        if (cookie != null) {
            final User existedUser = storage.getUser(cookie.getValue());
            if (existedUser != null) {
                log.info("User found, update");
                existedUser.setAvtoNumber(user.getAvtoNumber());
                existedUser.setName(user.getName());
                existedUser.setPhoneNumber(user.getPhoneNumber());
                existedUser.setSubscribeURL(user.getSubscribeURL());
                userId = storage.putUser(existedUser);
            } else {
                log.info("User not found, create new");
                userId = storage.putUser(user);
            }
        } else {
            log.info("Create new user");
            userId = storage.putUser(user);
        }

        final NewCookie newCookie = new NewCookie(SMART_PARKING_USER_ID_COOKIE, userId, "/", null, NewCookie.DEFAULT_VERSION, null, NewCookie.DEFAULT_MAX_AGE, null, false, false);
        log.info("set cookie: " + newCookie);
        return Response.ok().cookie(newCookie).build();
    }
}
