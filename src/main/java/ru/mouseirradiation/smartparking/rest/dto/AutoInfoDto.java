package ru.mouseirradiation.smartparking.rest.dto;

import java.io.Serializable;

public class AutoInfoDto implements Serializable {
    private String phoneNumber;
    private String autoNumber;

    public AutoInfoDto() {
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAutoNumber() {
        return autoNumber;
    }

    public void setAutoNumber(String autoNumber) {
        this.autoNumber = autoNumber;
    }

    @Override
    public String toString() {
        return "AutoInfoDto{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", autoNumber='" + autoNumber + '\'' +
                '}';
    }
}
