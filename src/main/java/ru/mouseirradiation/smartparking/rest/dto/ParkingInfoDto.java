package ru.mouseirradiation.smartparking.rest.dto;

import java.util.Date;
import java.util.List;

public class ParkingInfoDto {
    /**
     * Время когда пользователь планирует освободить парковочное место
     */
    private Date leave;

    /**
     * Список автомобилей(номеров) которых я блокирую
     * [0..N]
     */
    private List<String> lockAutoList;

    public ParkingInfoDto() {
    }

    public Date getLeave() {
        return leave;
    }

    public void setLeave(Date leave) {
        this.leave = leave;
    }

    public List<String> getLockAutoList() {
        return lockAutoList;
    }

    public void setLockAutoList(List<String> lockAutoList) {
        this.lockAutoList = lockAutoList;
    }

    @Override
    public String toString() {
        return "ParkingInfoDto{" +
                "leave=" + leave +
                ", lockAutoList=" + lockAutoList +
                '}';
    }
}
