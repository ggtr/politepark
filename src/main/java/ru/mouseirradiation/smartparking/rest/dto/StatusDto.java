package ru.mouseirradiation.smartparking.rest.dto;

import parking.store.Parking;
import parking.store.User;

import java.io.Serializable;
import java.util.List;

public class StatusDto implements Serializable {

    /**
     * Информация о текущем пользователе
     */
    private User user;

    /**
     * Информация о текущей парковке
     */
    private Parking parking;

    /**
     * Список авто которых заблокировал я
     * [0..n]
     */
    private User lockedByMe;

    /**
     * Список авто которые блокируют меня
     * [0..n]
     */
    private User lockingMe;

    public StatusDto() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Parking getParking() {
        return parking;
    }

    public void setParking(Parking parking) {
        this.parking = parking;
    }

    public User getLockedByMe() {
        return lockedByMe;
    }

    public void setLockedByMe(User lockedByMe) {
        this.lockedByMe = lockedByMe;
    }

    public User getLockingMe() {
        return lockingMe;
    }

    public void setLockingMe(User lockingMe) {
        this.lockingMe = lockingMe;
    }

    @Override
    public String toString() {
        return "StatusDto{" +
                "user=" + user +
                ", parking=" + parking +
                ", lockedByMe=" + lockedByMe +
                ", lockingMe=" + lockingMe +
                '}';
    }
}
