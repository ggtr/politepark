package ru.mouseirradiation.smartparking.rest.dto;

import java.io.Serializable;

public class UserDto implements Serializable {
    private AutoInfoDto userAuto;

    public UserDto() {
    }

    public AutoInfoDto getUserAuto() {
        return userAuto;
    }

    public void setUserAuto(AutoInfoDto userAuto) {
        this.userAuto = userAuto;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "userAuto=" + userAuto +
                '}';
    }
}
