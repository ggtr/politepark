package ru.mouseirradiation.smartparking.rest.dto;

import parking.store.Parking;
import parking.store.User;

public class UserParkingDto {
    private User user;
    private Parking parking;

    public UserParkingDto() {
    }

    public UserParkingDto(User user, Parking parking) {
        this.user = user;
        this.parking = parking;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Parking getParking() {
        return parking;
    }

    public void setParking(Parking parking) {
        this.parking = parking;
    }

    @Override
    public String toString() {
        return "UserParkingDto{" +
                "user=" + user +
                ", parking=" + parking +
                '}';
    }
}
