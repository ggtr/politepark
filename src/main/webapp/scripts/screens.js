function detectmob() {
    var callButton = document.getElementById("callButton");
    if (navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)) {

        callButton.style.display = "block";
        return true;
    } else {
        callButton.style.display = "none";
        return false;
    }
}

function getParameter(key) {
    var url = new URL(window.location.href);
    return url.searchParams.get(key);
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function checkRegistration() {
    //todo проверить что пользователь зарегистрирован
    // если не зарегин, отправлять на
    // window.location.href = 'registration.html';
}