package parking.store;

import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class StoreTest {

    private final LocalServiceTestHelper helper =
            new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

    @Before
    public void setUp() {
        helper.setUp();
    }

    @After
    public void tearDown() {
        helper.tearDown();
    }


    @Test
    public void testUser() throws EntityNotFoundException {
        Storage storage = new Storage();
        User user = new User("name", "916-11-11-11", "A556МР177", "url111", "pukKey");
        String key = storage.putUser(user);
        User user2 = storage.getUser(key);
        assertEquals(user2, user);
        storage.deleteEntityByKey(key);
    }

    @Test
    public void testUpdateUser() throws EntityNotFoundException {
        Storage storage = new Storage();
        User user = new User("name2", "916-11-11-11", "A556МР177", "url111", "pukKey");
        String key = storage.putUser(user);

        User user2 = storage.getUser(key);

        String r = new Date().toString();

        user2.setName(r);
        storage.putUser(user2);

        User user3 = storage.getUser(key);
        assertEquals(r, user3.getName());

        storage.deleteEntityByKey(key);
    }


    @Test
    public void testParking() throws EntityNotFoundException {
        Storage storage = new Storage();

        Parking p = new Parking("from1", "to2", new Date());
        String key = storage.putParking(p);

        Parking p2 = storage.getParking(key);
        assertEquals(p.getFromUserKey(),p2.getFromUserKey());
        assertEquals(p.getToUserKey(),  p2.getToUserKey());

        storage.deleteEntityByKey(KeyFactory.keyToString(p2.getKey()));

    }


    @Test
    public void testParkingUpdate() throws EntityNotFoundException {
        Storage storage = new Storage();

        Parking p = new Parking("from1-5", "to2-5", new Date());
        String key = storage.putParking(p);

        Date d = new Date();
        Parking p2 = storage.getParking(key);
        p2.setOutTime(d);
        storage.putParking(p2);

        Parking p3 = storage.getParking(key);
        assertEquals(p3.getOutTime(), d);
    }



    //  выезд <- key3  key2 key1   | тупик
    @Test
    public void testParkingFrom(){
        Storage storage = new Storage();

        Parking p1 = new Parking("key1", new Date());
        Parking p2 = new Parking("key2", "key1", new Date());
        Parking p3 = new Parking("key3", "key2", new Date());

        String key1 = storage.putParking(p1);
        String key2 = storage.putParking(p2);
        String key3 = storage.putParking(p3);


        // кто блокирует key1
        String res1 = storage.getFromParkings("key1");
        assertEquals("key2", res1);

        // кто блокирует key 2
        String res2 = storage.getFromParkings("key2");
        assertEquals("key3", res2);

        // кто блокирует key 3
        String res3 = storage.getFromParkings("key3");
        assertEquals(null, res3);

        storage.deleteEntityByKey(key1);
        storage.deleteEntityByKey(key2);
        storage.deleteEntityByKey(key3);

    }


    //  выезд <- key3  key2 key1   | тупик
    @Test
    public void testParkingTo(){
        Storage storage = new Storage();

        Parking p1 = new Parking("key1", new Date());
        Parking p2 = new Parking("key2", "key1", new Date());
        Parking p3 = new Parking("key3", "key2", new Date());

        String key1 = storage.putParking(p1);
        String key2 = storage.putParking(p2);
        String key3 = storage.putParking(p3);


        // кого блокирует key1
        String res1 = storage.getToParkings("key1");
        assertEquals(null, res1);

        // кого блокирует key 2
        String res2 = storage.getToParkings("key2");
        assertEquals("key1", res2);

        // кого блокирует key 3
        String res3 = storage.getToParkings("key3");
        assertEquals("key2", res3);

        storage.deleteEntityByKey(key1);
        storage.deleteEntityByKey(key2);
        storage.deleteEntityByKey(key3);

    }



    //  выезд <- key3  key2 key1   | тупик
    @Test
    public void testParkingDelete(){
        Storage storage = new Storage();

        Parking p1 = new Parking("key1", new Date());
        Parking p2 = new Parking("key2", "key1", new Date());
        Parking p3 = new Parking("key3", "key2", new Date());

        String key1 = storage.putParking(p1);
        String key2 = storage.putParking(p2);
        String key3 = storage.putParking(p3);


        storage.deleteParking("key1");
        assertEquals(null, storage.getToParkings("key1"));

        storage.deleteParking("key2");
        assertEquals(null, storage.getToParkings("key2"));

        storage.deleteParking("key3");
        assertEquals(null, storage.getToParkings("key3"));

        storage.deleteEntityByKey(key1);
        storage.deleteEntityByKey(key2);
        storage.deleteEntityByKey(key3);

    }


    //  выезд <- key3  key2 key1   | тупик
    @Test
    public void testGetUserParking(){
        Storage storage = new Storage();

        Parking p1 = new Parking("key1", new Date());
        Parking p2 = new Parking("key2", "key1", new Date());
        Parking p3 = new Parking("key3", "key2", new Date());

        String key1 = storage.putParking(p1);
        String key2 = storage.putParking(p2);
        String key3 = storage.putParking(p3);


        Parking p1_1 = storage.getUserParking("key1");
        assertEquals(p1, p1_1);

        Parking p2_1 = storage.getUserParking("key2");
        assertEquals(p2, p2_1);

        Parking p3_1 = storage.getUserParking("key3");
        assertEquals(p3, p3_1);

        storage.deleteEntityByKey(key1);
        storage.deleteEntityByKey(key2);
        storage.deleteEntityByKey(key3);
    }


}
